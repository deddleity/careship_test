<?php
namespace App\Exceptions;

use UnexpectedValueException;

class NoteAvailableException extends UnexpectedValueException
{

}