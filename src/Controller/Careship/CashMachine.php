<?php

namespace App\Controller\Careship;
/**
 * Created by PhpStorm.
 * User: diana
 * Date: 26/03/18
 * Time: 05:47 PM
 */
use App\Utils\CashCalculator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use App\Validations\CashMachineRequestValidation;


/**
 * Cash machine controller.
 *
 * @Route("/")
 */
class CashMachine extends Controller
{
    /**
     * Get required notes
     *
     * @FOSRest\Post("/")
     * @param Request $request
     * @return Response
     *
     */
    public function postNotesAction(Request $request)
    {
        $content = $request->request->all();

        $validation = new CashMachineRequestValidation;
        $validation->validate($content['request']);

        $calculator = new CashCalculator();

        $response = $calculator->getOrderedResults($content['request']);

        return new Response(json_encode($response), 200, array("content-type" =>"application/json"));
    }

    /**
     * Get instructions
     *
     * @FOSRest\Get("/")
     * @return Response
     *
     */
    public function notesResponse()
    {
        $instructions = 'by post method send a request number multiple of ten';

        return new Response($instructions, 200,  array("content-type" =>"application/json"));

    }
}