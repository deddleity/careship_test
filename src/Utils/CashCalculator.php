<?php
namespace App\Utils;
/**
 * Created by PhpStorm.
 * User: diana
 * Date: 26/03/18
 * Time: 07:43 PM
 */

class CashCalculator
{
    /**
     * @var array
     */
    protected $notes = [100, 50, 20, 10];

    public function calculator($amount)
    {
        $result = array();

        foreach ($this->notes as $note)
        {
            $result[$note] = intval($amount / $note);
            $amount       %= $note;
        }

        return $result;
    }

    /**
     * @param $amount
     * @return array
     */
    public function getOrderedResults($amount)
    {
        $result = array();

        if ($amount === 0) {
            return $result;
        }

        $calculator = $this->calculator($amount);

        foreach ($calculator as $key => $value) {
            while ($value > 0) {
                array_push($result,$key);
                $value --;
            }
        }
        return $result;
    }
}