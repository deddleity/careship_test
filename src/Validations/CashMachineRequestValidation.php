<?php
/**
 * Created by PhpStorm.
 * User: diana
 * Date: 26/03/18
 * Time: 06:35 PM
 */
namespace App\Validations;

use App\Exceptions\InvalidArgumentException;
use App\Exceptions\NoteAvailableException;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Type;

class CashMachineRequestValidation
{
    /**
     * @var Validation
     */
    protected $validator;

    public function __construct()
    {
        $this->validator = Validation::createValidator();
    }

    /**
     * @param $contentRequest
     * @return bool
     */
    public function validate($contentRequest)
    {
        $numeric = $this->validator->validate($contentRequest, array(
            new Type('numeric'),
            new GreaterThanOrEqual(0)
        ));

        if(count($numeric) === 0)
        {
            $requestValue = round(floatval($contentRequest), 0, PHP_ROUND_HALF_UP);

            if($requestValue !== $contentRequest){
                Throw new NoteAvailableException('Please do not include a decimal Value');
            } else {
                 $intValue = intval($contentRequest);
                if (!is_int($intValue / 10)) {
                    Throw new NoteAvailableException('Please put a value multiple of ten');
                };
            }

        } else {

            Throw new InvalidArgumentException('Please put a natural number multiple of ten');
        }

        return $contentRequest;
    }
}