<?php
namespace AppApp\Tests\Careship;
/**
 * Created by PhpStorm.
 * User: diana
 * Date: 26/03/18
 * Time: 06:23 PM
 */
use App\Exceptions\InvalidArgumentException;
use App\Controller\Careship\CashMachine;
use App\Exceptions\NoteAvailableException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CashMachineTest extends WebTestCase
{
    /**
     * @expectedException
     */
    public function testNegativeNumbersRiseInvalidException()
    {
        $this->expectException(InvalidArgumentException::class);
        $machine = new CashMachine();

        $client = static::createClient();

        $client->request(
            'POST', '/', array(), array(),
            array(
                'CONTENT_TYPE' => 'application/json',
            ),
            '{"request": -50}'
        );

        $machine->postNotesAction($client->getRequest());
    }

    /**
     * @expectedException
     */
    public function testNotTenRoundNumbersRiseNoteException()
    {
        $this->expectException(NoteAvailableException::class);
        $machine = new CashMachine();

        $client = static::createClient();

        $client->request(
            'POST', '/', array(), array(),
            array(
                'CONTENT_TYPE' => 'application/json',
            ),
            '{"request": 123.00}'
        );

        $machine->postNotesAction($client->getRequest());
    }

    /**
     * @expectedException
     */
    public function testDecimalValueRiseNoteException()
    {
        $this->expectException(NoteAvailableException::class);
        $machine = new CashMachine();

        $client = static::createClient();

        $client->request(
            'POST', '/', array(), array(),
            array(
                'CONTENT_TYPE' => 'application/json',
            ),
            '{"request": 120.20}'
        );

        $machine->postNotesAction($client->getRequest());
    }

}